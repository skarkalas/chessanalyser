create table games
(
	hash number,
	event varchar2(100),
	site varchar2(100),
	date_ varchar2(10),
	round_ varchar2(30),
	white varchar2(50),
	black varchar2(50),
	result_ varchar2(10),
	w_elo varchar2(10),
	b_elo varchar2(10)	
)
segment creation immediate;

alter table games add constraint games_hash_pk primary key (hash);
alter table games add constraint games_event_nn check (event is not null);
alter table games add constraint games_site_nn check (site is not null);
alter table games add constraint games_date_nn check (date_ is not null);
alter table games add constraint games_round_nn check (round_ is not null);
alter table games add constraint games_white_nn check (white is not null);
alter table games add constraint games_black_nn check (black is not null);
alter table games add constraint games_result_nn check (result_ is not null);

create table analyses
(
	id number,
	game_id number,
	date_ timestamp default systimestamp,
	player varchar2(5),
	multipv number,
	time_ number,
	depth_ number,
	moves number
)
segment creation immediate;

alter table analyses add constraint analyses_id_pk primary key (id);
alter table analyses add constraint analyses_game_id_nn check (game_id is not null);
alter table analyses add constraint analyses_date_nn check (date_ is not null);
alter table analyses add constraint analyses_player_nn check (player is not null);
alter table analyses add constraint analyses_player_limit check (player in ('white', 'black', 'both'));
alter table analyses add constraint analyses_multipv_nn check (multipv is not null);
alter table analyses add constraint analyses_multipv_limit check (multipv between 1 and 10);
alter table analyses add constraint analyses_time_nn check (time_ is not null);
alter table analyses add constraint analyses_time_limit check (time_ between 0 and 5000);
alter table analyses add constraint analyses_depth_nn check (depth_ is not null);
alter table analyses add constraint analyses_depth_limit check (depth_ between 2 and 20);
alter table analyses add constraint analyses_moves_nn check (moves is not null);
alter table analyses add constraint analyses_moves_limit check (moves > 2);

alter table analyses add constraint analyses_game_id_fk foreign key (game_id) references games (hash) on delete cascade;

create sequence analyses_seq minvalue 1 start with 1 increment by 1 order nocache;

create table results
(
	id number,
	analysis_id number,
	player_move varchar2(10),
	value_ number,
	rank_ number,
	player varchar2(5),
	pieces number,
	move_no number
)
segment creation immediate;

alter table results add constraint results_id_pk primary key (id);
alter table results add constraint results_analysis_id_nn check (analysis_id is not null);
alter table results add constraint results_player_move_nn check (player_move is not null);
alter table results add constraint results_value_nn check (value_ is not null);
alter table results add constraint results_rank_nn check (rank_ is not null);
alter table results add constraint results_rank_limit check (rank_ > 0);
alter table results add constraint results_player_limit check (player in ('white', 'black'));
alter table results add constraint results_pieces_limit check (pieces between 1 and 32);
alter table results add constraint results_move_no_limit check (move_no > 0);

alter table results add constraint results_analysis_id_fk foreign key (analysis_id) references analyses (id) on delete cascade;

create sequence results_seq minvalue 1 start with 1 increment by 1 order nocache;

create table suggestions
(
	id number,
	result_id number,
	engine_move varchar2(10),
	depth_ number,	
	value_ number,
	rank_ number
)
segment creation immediate;

alter table suggestions add constraint suggestions_id_pk primary key (id);
alter table suggestions add constraint suggestions_result_id_nn check (result_id is not null);
alter table suggestions add constraint suggestions_engine_move_nn check (engine_move is not null);
alter table suggestions add constraint suggestions_depth_nn check (depth_ is not null);
alter table suggestions add constraint suggestions_depth_limit check (depth_ between 1 and 20);
alter table suggestions add constraint suggestions_value_nn check (value_ is not null);
alter table suggestions add constraint suggestions_rank_nn check (rank_ is not null);
alter table suggestions add constraint suggestions_rank_limit check (rank_ > 0 or rank_ = -1);

alter table suggestions add constraint suggestions_result_id_fk foreign key (result_id) references results (id) on delete cascade;

create sequence suggestions_seq minvalue 1 start with 1 increment by 1 order nocache;
