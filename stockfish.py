import subprocess, time

class Engine:
    def __init__(self, filename):
        self.initialize(filename)

    def initialize(self, filename):
        if (filename == ""):
            self.engine = None
            return		
		
        self.engine = subprocess.Popen(filename,universal_newlines=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,)
        self.get()
        self.put('uci')
        self.get()
        self.setOption('Hash', 128)
        self.get()

    def setOption(self, name, value):
        if (self.engine == None):
            return		
        self.put('setoption name ' + name + ' value ' + str(value))
		
    def put(self, command):
        if (self.engine == None):
            return		
        #print('\nyou:\n\t'+command)
        self.engine.stdin.write(command+'\n')

    def get(self):
        if (self.engine == None):
            return
        # display results if machine 'isready' to respond 'readyok'
        self.engine.stdin.write('isready\n')
        #print('\nengine:')
        while True:
            text = self.engine.stdout.readline().strip()
            if text == 'readyok':
                break
            if text !='':
                pass
                #print('\t'+text)

    def search(self, position, multipv=1, depth=0, movetime=0, threads=1):
        if (self.engine == None):
            return None
        self.setOption('MultiPV', multipv)
        self.setOption('Threads', threads)
        self.get()
        self.put('position startpos moves ' +  position)
        self.get()
        command = 'go '
        if (depth != 0):
            command = command + 'depth ' + str(depth)
        elif (movetime != 0):
            command = command + 'movetime ' + str(movetime)
        self.put(command)
        topMovesforNext = []
        movesDepthMinusOne = []
        otherMovesforNext = []
        stop = False
        while stop == False:
            text = self.engine.stdout.readline().strip()
            if text.find('bestmove') != -1:
                stop = True
            if text !='':
                #print('\t'+text)
                move = self.extractInfo(text)
                #print 'move:', move, text
                if move != None:				
                    if move[2] == depth:
                        topMovesforNext.append(move)
                    else:
                        if move[2] == depth - 1:
                            movesDepthMinusOne.append(move)				
                        otherMovesforNext.append(move)
            time.sleep(10.0/1000.0)
        topMovesforNext = sorted(topMovesforNext, key=lambda x: x[0], reverse=True)
        topMovesforNext = zip(range(1, multipv + 1), topMovesforNext[:multipv])
        movesDepthMinusOne = sorted(movesDepthMinusOne, key=lambda x: x[0], reverse=True)
        movesDepthMinusOne = zip(range(1, multipv + 1), movesDepthMinusOne[:multipv])
        if movesDepthMinusOne == []:
            value = 0
        else:
            value = movesDepthMinusOne[0][1][0] * -1
        return (value, topMovesforNext, otherMovesforNext)
		
    def extractInfo(self, line):
        if (line.find(' cp ') == -1 and line.find(' mate ') == -1) or line.find(' multipv ') == -1:
            #print('*** excluded text: ', line)
            return None
        line = line.split(' depth ')[1].strip()
        depth = int(line.split(' ')[0])
        if line.find(' cp ') == -1:		
            line = line.split(' mate ')[1].strip()
        else:
            line = line.split(' cp ')[1].strip()
        cp = int(line.split(' ')[0])
        line = line.split(' pv ')[1].strip()
        pv = line.split(' ')[0]
        return (cp, pv, depth)
		
    def evaluate(self, moves, move):
        value = None
        values = [x[1][0] for x in moves if x[1][1]==move]
        if values != []: value = values[0]
        return value

    def process(self, position, multipv=1, depth=0, movetime=0, threads=1):
        if (self.engine == None):
            return None
        #get value for this move
        #search for best n moves for next player (topMovesforNext)
        #search for rest of moves for next player (otherMovesforNext)
        value, topMovesforNext, otherMovesforNext = self.search(position, multipv, depth, movetime, threads)
        #print "computer moves: ", topMoves

        #rank move actually played
        #rank = self.rankMove(topMovesforThis, playerMove)

        # *** disabled for consistency in evaluation -------------------------------------
        #evaluate move actually played
        #value = self.evaluate(topMoves, playerMove)
        #if move is not one of the computer suggested ones, play it and use the complement of the best opponent move
        #if value == None:
        # *** disabled for consistency in evaluation -------------------------------------
		
        #evaluate move - play the move
        #position = position + ' ' + playerMove
        #search for best n moves for opponent
        #opponentMoves, _ = self.search(position, multipv, depth-1, movetime)
        #evaluate player move using (best opponent cp * -1)
        #print 'opponentMoves', opponentMoves
        #if opponentMoves == []:
        #    value = 0
        #else:
        #    value = opponentMoves[0][1][0] * -1
        playerMove = position.split(' ')[-1]
        return ((value, playerMove), topMovesforNext, otherMovesforNext)
		
    def toString(self, data):
        playerMove = data[0]
        output = 'Results\n'
        output += '======\n'
        output += 'player move: ' + playerMove[1][1] + '\n'
        output += 'value in cp: ' + str(playerMove[1][0]) + '\n'
        output += 'rank: ' + str(playerMove[0]) + '\n\n'
        computerMoves = data[1]
        for item in computerMoves:
            output += 'computer move: ' + item[1][1] + '\n'
            output += 'value in cp: ' + str(item[1][0]) + '\n'
            output += 'rank: ' + str(item[0]) + '\n\n'
        return output

    def analyse(self, moves, move, multipv=1):
        if (self.engine == None):
            return
        self.setOption('MultiPV', multipv)
        self.get()
        self.put('position startpos moves ' + moves)
        self.get()
        self.put('go infinite')
        possibleMoves = []
        stop = False
        engineMove = ()
        while stop == False:
            text = self.engine.stdout.readline().strip()
            if text !='':
                print('\t'+text)
                currentMove = self.extractInfo(text)
                if currentMove != None:
                    if currentMove[2] == move:
                        engineMove = currentMove[1:3]
                        stop = True
            time.sleep(10.0/1000.0)
        self.put('stop')
        return engineMove
		
def main():
    engine = Engine('stockfish.exe')
    engine.put('ucinewgame')
    engine.get()
    player, topMovesforNext, otherMovesforNext = engine.process('e2e4', 5, 3, 0, 4)
    print 'player:', player
    print 'top for next:', topMovesforNext
    print 'other for next:', otherMovesforNext
    #start_time = time.time()
    #print(engine.search('e2e4', 5, 20, 0, 4))
    #print("--- %s seconds ---" % (time.time() - start_time))
    #result = engine.process(position='e2e4 e7e6 d2d4 d7d5', playerMove='b1d2', multipv=5, depth=3)
    #print(engine.process(position='e2e4 e7e6 d2d4 d7d5', playerMove='b1d2', multipv=5, depth=3))
    #print(engine.process(position='e2e4 d7d5 e4d5 d8d5 b1c3 d5d8 f1c4 g8f6 g1f3 c8g4 h2h3 g4f3 d1f3 e7e6 f3b7 b8d7 c3b5 a8c8 b5a7 d7b6 a7c8 b6c8 d2d4 c8d6 c4b5 d6b5 b7b5 f6d7 d4d5 e6d5 c1e3 f8d6 a1d1 d8f6 d1d5 f6g6 e3f4 d6f4 b5d7 e8f8', playerMove='d7d8', multipv=5, depth=3))
    #result = engine.process(position='e2e4 c7c5', playerMove='g1f3', multipv=5, depth=3)
    #print(engine.toString(result))
    #print(engine.search('e2e4 e7e6 d2d4 d7d5 b1d2', 5, 3))
    engine.put('quit')

if __name__ == "__main__":
    main()
				
				
				

