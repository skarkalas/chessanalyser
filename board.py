class Board:
    def __enter__(self):
        self.board = {}
        for key, value in self.board_setup():
            self.board[key] = value
        #print('board is created')
        return self
		
    def __exit__(self, type, value, traceback):
        self.board = {}
        #print('board is deleted')

    def board_setup(self):
        yield ("a1", "w_rook")
        yield ("b1", "w_knight")
        yield ("c1", "w_bishop")
        yield ("d1", "w_queen")
        yield ("e1", "w_king")
        yield ("f1", "w_bishop")
        yield ("g1", "w_knight")
        yield ("h1", "w_rook")
        alphabet = xrange(ord('a'), ord('h')+1)
        for c in alphabet:
            yield (chr(c) + "2", "w_pawn")
            yield (chr(c) + "7", "b_pawn")
        for i in xrange(3, 7):
            for c in alphabet:
                yield (chr(c) + str(i), "")
        yield ("a8", "b_rook")
        yield ("b8", "b_knight")
        yield ("c8", "b_bishop")
        yield ("d8", "b_queen")
        yield ("e8", "b_king")
        yield ("f8", "b_bishop")
        yield ("g8", "b_knight")
        yield ("h8", "b_rook")
        
    def set_state(self, plies):
        plies = plies.split(" ")
        for ply in plies:
            begin = ply[0:2]
            end = ply[2:4]
            piece = self.board[begin]
            self.board[begin] = ""
            self.board[end] = piece

    def get_no_of_pieces(self):
        report = 0
        for i in xrange(8, 0, -1):
            for c in xrange(ord('a'), ord('h')+1):
                report += 0 if self.board[chr(c) + str(i)] == "" else 1
        return report

    def get_pieces(self):
        white = []
        black = []
        for i in xrange(8, 0, -1):
            for c in xrange(ord('a'), ord('h')+1):
                piece = self.board[chr(c) + str(i)]
                if piece[0:1] == "w":
                    white.append(piece)
                if piece[0:1] == "b":
                    black.append(piece)
        return (white, black)

    def get_white_pieces(self):
        white, _ = self.get_pieces()
        return white

    def get_no_of_white_pieces(self):
        return len(self.get_white_pieces())

    def get_black_pieces(self):
        _, black = self.get_pieces()
        return black

    def get_no_of_black_pieces(self):
        return len(self.get_black_pieces())

    def __str__(self):
        report = ''
        for i in xrange(8, -1, -1):
            report += str(i) + " "
            for c in xrange(ord('a'), ord('h')+1):
                if i == 0:
                    report += "%10s" % chr(c)
                else:
                    report += "%10s" % self.board[chr(c) + str(i)]
            report += "\n"
        return report

def main():
    with Board() as board:
        print board
        plies = "e2e4 e7e6 d2d4 d7d5 b1d2 e4d5"
        board.set_state(plies)
        print board.get_no_of_pieces()
        print board.get_pieces()
        print board.get_white_pieces()
        print board.get_black_pieces()
        print board.get_no_of_white_pieces()
        print board.get_no_of_black_pieces()
        print board

if __name__ == "__main__":
    main()
				
				
