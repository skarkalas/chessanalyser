class Game:
    def __init__(self, data):
        self.initialize(data)

    def initialize(self, data):
        self.id = 0
        if (len(data) != 2):
            self.info = []
            self.moves = []
            return
		#get game info
        self.setInfo(data)
		#get game moves
        self.setMoves(data)
        #compute id
        self.id = hash(''.join(data[0])+''.join(data[1]))

    def setInfo(self, data):
        self.info = data[0]
        self.info = [item.strip('[').strip(']') for item in self.info]
        self.info = [[word.strip('"') for word in item.split()] for item in self.info]
        self.info = [(item[0], ' '.join(item[1:len(item)])) for item in self.info]
        info = {}
        for item in self.info:
            info[item[0]] = item[1]
        self.info = info
		
    def getInfo(self, key):
        try:
            value = self.info[key]
        except:
            print("no value exists for the key=", key)
            return ""
        else:
            return value

    def getBasicInfo(self):
        info = []
        info.append(self.id)
        info.append(self.getInfo('Event'))
        info.append(self.getInfo('Site'))
        info.append(self.getInfo('Date'))
        info.append(self.getInfo('Round'))
        info.append(self.getInfo('White'))
        info.append(self.getInfo('Black'))
        info.append(self.getInfo('Result'))
        info.append(self.getInfo('WhiteElo'))
        info.append(self.getInfo('BlackElo'))
        return tuple(info)
	
    def setMoves(self, data):
        self.moves = data[1]
        self.moves = ' '.join(self.moves)
        self.moves = self.moves.split(' ')
        self.moves.pop()
        if len(self.moves) % 2 == 1:
            self.moves.append('')
        self.moves = [(self.moves[i],self.moves[i+1]) for i in xrange(0,len(self.moves),2)]
        self.createIterator()
		
    def createIterator(self):
        self.iterator = self.getIterator()
		
    def getIterator(self):
        for item in self.moves:
            yield item
			
    def nextMove(self):
        try:
            move = next(self.iterator)
        except:
            print("run out of moves!")
            return ()
        else:
            return move
			
    def __str__(self):
        info = ""
        for key, value in self.info.iteritems():
            info += "%s = %s\n" % (key, value)
        moves = "\n".join([str(tuple) for tuple in self.moves])
        return info + "\n" + moves

class GameTokenizer:
    def __init__(self, data):
        self.initialize(data)

    def initialize(self, data):
        #get list of lines
        self.lines = data.splitlines()
        self.lengths = [len(line) for line in self.lines]
        self.position = 0
		
    def reset(self):
        self.position = 0
		
    def hasMore(self):
        return self.position < len(self.lengths) - 1

    def next(self):		
        #get info and moves as a tuple
        try:
            game = (self.nextPart(), self.nextPart())
        except:
            print("next game cannot be extracted - file format problem")
            return ()
        else:
            return game
			
    def nextPart(self):
		#find index of next empty line
        start = self.position
        end = self.nextIndex(start, 0)

        #if next part cannot be found raise exception
        if (end == start):
            raise "Invalid file format!", end
            self.position = len(self.lengths) - 1

        self.position = end + 1
        return self.lines[start:end]

    def nextIndex(self, position, item):
        stop = False
        while (stop == False and position < len(self.lengths)):
            if (self.lengths[position] == item):
                stop = True
            else: position += 1
        return position
				
def main():
    text = """[Event "Lloyds Bank op"]
[Site "London"]
[Date "1984.??.??"]
[Round "1"]
[White "Adams, Michael"]
[Black "Sedgwick, David"]
[Result "1-0"]
[WhiteElo ""]
[BlackElo ""]
[ECO "C05"]

1.e4 e6 2.d4 d5 3.Nd2 Nf6 4.e5 Nfd7 5.f4 c5 6.c3 Nc6 7.Ndf3 cxd4 8.cxd4 f6
9.Bd3 Bb4+ 10.Bd2 Qb6 11.Ne2 fxe5 12.fxe5 O-O 13.a3 Be7 14.Qc2 Rxf3 15.gxf3 Nxd4
16.Nxd4 Qxd4 17.O-O-O Nxe5 18.Bxh7+ Kh8 19.Kb1 Qh4 20.Bc3 Bf6 21.f4 Nc4 22.Bxf6 Qxf6
23.Bd3 b5 24.Qe2 Bd7 25.Rhg1 Be8 26.Rde1 Bf7 27.Rg3 Rc8 28.Reg1 Nd6 29.Rxg7 Nf5
30.R7g5 Rc7 31.Bxf5 exf5 32.Rh5+  1-0
"""
    tokenizer = GameTokenizer(text)
    token = tokenizer.next()
    print(hash(''.join(token[0])+''.join(token[1])))

if __name__ == "__main__":
    main()				
				
				
				
				
				
				
				
				
				
				