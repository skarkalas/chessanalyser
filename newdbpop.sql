insert into games values (4555445556,'Lloyds Bank op','London', '1984.??.??', '1', 'Adams, Michael', 'Sedgwick, David', '1-0');

insert into analyses (id, game_id, player, multipv, _time, _depth, moves) values (analyses_seq.nextval, 1, 'white', 5, 0, 3, 5);

insert into results values (results_seq.nextval, 1, 'e4e5', 53, 4);

insert into suggestions values (suggestions_seq.nextval, 1, 'f1e5', 87, 1);
insert into suggestions values (suggestions_seq.nextval, 1, 'c1d2', 65, 2);
insert into suggestions values (suggestions_seq.nextval, 1, 'f2f4', 60, 3);
insert into suggestions values (suggestions_seq.nextval, 1, 'e4e5', 53, 4);
insert into suggestions values (suggestions_seq.nextval, 1, 'd8b6', -12, 5);

