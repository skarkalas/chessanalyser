from Tkinter import *
from Tix import *
from game import *
from stockfish import *
from oracle import *
from board import *
from datetime import datetime
from subprocess import Popen, PIPE
import tkFileDialog
import shlex
import time
import thread

class MainApp(Tk):
    def __init__(self,parent=NONE):
        Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        # instantiate the stockfish engine
        self.engine = Engine('stockfish.exe')
		
        # list of games in the current session
        self.games = []
		
		# size the main window and put it in the centre
        self.width = 800
        self.height = 400
        self.center()
        self.resizable(0,0)

		# create a toplevel menu
        menubar = Menu(self)

		# create a File menu, and add it to the menu bar
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open", command=self.onOpen)
        filemenu.add_command(label="Save", command=self.onSave)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.quit)
        menubar.add_cascade(label="File", menu=filemenu)

		# create a Help menu, and add it to the menu bar
        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About", command=self.onAbout)
        menubar.add_cascade(label="Help", menu=helpmenu)

		# display the menu
        self.config(menu=menubar)
		
		# attach the tool frame
        self.toolPage = ToolPage(self)
        self.toolPage.place(x=0, y=0, anchor=NW)

    def center(self):
        ws = self.winfo_screenwidth()
        hs = self.winfo_screenheight()
        x = ws/2 - self.width/2
        y = hs/2 - self.height/2
        self.geometry('%dx%d+%d+%d' % (self.width, self.height, x, y))	
		
    def onAbout(self):
        pass

    def onSave(self):
        pass
		
    def onOpen(self):
        ftypes = [('Portable Game Notation files', '*.pgn'), ('All files', '*')]
        dialog = tkFileDialog.Open(self, filetypes = ftypes)
        file = dialog.show()
        if file != '':
            #text = self.readFile(file)
            exitcode, text, err = self.parseAndReadFile('pgn -Wuci ' + file)
            if (text == ""):
                print("problem with file format")
                pass
            tokenizer = GameTokenizer(text)
            self.games = []			
            while (tokenizer.hasMore()):
                self.games.append(Game(tokenizer.next()))
        else:
            return
        self.toolPage.resetControls()
        self.toolPage.updateEvents(self.getDistinctInfoItems('Event'))
        self.toolPage.updateSites(self.getDistinctInfoItems('Site'))
        self.toolPage.updateGames()

    def parseAndReadFile(self, command):
        args = shlex.split(command)
        proc = Popen(args, stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        exitcode = proc.returncode
        return exitcode, out, err
		
    def readFile(self, filename):
        file = open(filename, "r")
        text = file.read()
        file.close()
        return text

    def getDistinctInfoItems(self, key):
        items = [game.getInfo(key) for game in self.games]
        items = set(items)
        return items		

class ToolPage(Frame):
    def __init__(self,parent):
        Frame.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        # configure options
        self.config(background="#CCFF99", width="800", height="400", relief=FLAT, borderwidth=1)
		
		# create a listbox for events and corresponding helper controls
        self.eventLst = Listbox(self)
        self.eventLst.place(x=15, y=25, anchor=NW)
        self.eventBtn = Button(self, text=u"apply filter", command=self.onEventBtnClick, width=16)
        self.eventBtn.place(x=15, y=200, anchor=NW)
        self.allgamesBtn = Button(self, text=u"all games", command=self.onAllGamesBtnClick, width=16)
        self.allgamesBtn.place(x=15, y=235, anchor=NW)
        self.eventLbl = Label(self, text="Events", background="#CCFF99")
        self.eventLbl.place(x=15, y=5, anchor=NW)
        self.selectedEvent = ""
		
		# create a listbox for sites and corresponding helper controls
        self.siteLst = Listbox(self)
        self.siteLst.place(x=150, y=25, anchor=NW)
        self.siteBtn = Button(self, text=u"apply filter", command=self.onSiteBtnClick, width=16)
        self.siteBtn.place(x=150, y=200, anchor=NW)
        self.siteLbl = Label(self, text="Sites", background="#CCFF99")
        self.siteLbl.place(x=150, y=5, anchor=NW)
        self.selectedSite = ""

        self.commonPlayerLbl = Label(self, text="Common Player:", background="#CCFF99")
        self.commonPlayerLbl.place(x=180, y=240, anchor=NW)
        self.commonPlayerVar = StringVar()
        self.commonPlayerTxf = Entry(self, textvariable=self.commonPlayerVar, justify=CENTER)
        self.commonPlayerTxf.place(x=285, y=240, anchor=NW)

        self.allGamesVar = IntVar()
        self.allGamesCbx = Checkbutton(self, variable=self.allGamesVar, onvalue=1, offvalue=0, text="Analyse all the games", background="#CCFF99")
        self.allGamesCbx.place(x=270, y=270, anchor=NW)
		
		# create a listbox for games and corresponding helper controls
        self.gameLst = Listbox(self)
        self.gameLst.place(x=285, y=25, anchor=NW)
        self.gameBtn = Button(self, text=u"show details", command=self.onGameBtnClick, width=16)
        self.gameBtn.place(x=285, y=200, anchor=NW)
        self.gameLbl = Label(self, text="Games", background="#CCFF99")
        self.gameLbl.place(x=285, y=5, anchor=NW)
        self.selectedGame = ""

		# create the controls for the game details
        self.gameDetailsLbl = Label(self, text="Game Details", background="#CCFF99")
        self.gameDetailsLbl.place(x=480, y=5, anchor=NW)

        self.gameEventLbl = Label(self, text="Event:", background="#CCFF99")
        self.gameEventLbl.place(x=420, y=30, anchor=NW)
        self.gameEventVar = StringVar()
        self.gameEventTxf = Entry(self, textvariable=self.gameEventVar, justify=CENTER)
        self.gameEventTxf.place(x=480, y=30, anchor=NW)
		
        self.gameSiteLbl = Label(self, text="Site:", background="#CCFF99")
        self.gameSiteLbl.place(x=420, y=60, anchor=NW)
        self.gameSiteVar = StringVar()
        self.gameSiteTxf = Entry(self, textvariable=self.gameSiteVar, justify=CENTER)
        self.gameSiteTxf.place(x=480, y=60, anchor=NW)

        self.gameDateLbl = Label(self, text="Date:", background="#CCFF99")
        self.gameDateLbl.place(x=420, y=90, anchor=NW)
        self.gameDateVar = StringVar()
        self.gameDateTxf = Entry(self, textvariable=self.gameDateVar, justify=CENTER)
        self.gameDateTxf.place(x=480, y=90, anchor=NW)

        self.gameRoundLbl = Label(self, text="Round:", background="#CCFF99")
        self.gameRoundLbl.place(x=420, y=120, anchor=NW)
        self.gameRoundVar = StringVar()
        self.gameRoundTxf = Entry(self, textvariable=self.gameRoundVar, justify=CENTER)
        self.gameRoundTxf.place(x=480, y=120, anchor=NW)

        self.gameWhiteLbl = Label(self, text="White:", background="#CCFF99")
        self.gameWhiteLbl.place(x=420, y=150, anchor=NW)
        self.gameWhiteVar = StringVar()
        self.gameWhiteTxf = Entry(self, textvariable=self.gameWhiteVar, justify=CENTER)
        self.gameWhiteTxf.place(x=480, y=150, anchor=NW)

        self.gameBlackLbl = Label(self, text="Black:", background="#CCFF99")
        self.gameBlackLbl.place(x=420, y=180, anchor=NW)
        self.gameBlackVar = StringVar()
        self.gameBlackTxf = Entry(self, textvariable=self.gameBlackVar, justify=CENTER)
        self.gameBlackTxf.place(x=480, y=180, anchor=NW)

        self.gameResultLbl = Label(self, text="Date:", background="#CCFF99")
        self.gameResultLbl.place(x=420, y=210, anchor=NW)
        self.gameResultVar = StringVar()
        self.gameResultTxf = Entry(self, textvariable=self.gameResultVar, justify=CENTER)
        self.gameResultTxf.place(x=480, y=210, anchor=NW)

		# create a listbox for moves and corresponding helper controls
        self.moveLst = Listbox(self)
        self.moveLst.place(x=665, y=25, anchor=NW)
        self.moveLbl = Label(self, text="Moves", background="#CCFF99")
        self.moveLbl.place(x=665, y=5, anchor=NW)
		
        self.moveCountLbl = Label(self, text="Count:", background="#CCFF99")
        self.moveCountLbl.place(x=615, y=210, anchor=NW)
        self.moveCountVar = StringVar()
        self.moveCountTxf = Entry(self, textvariable=self.moveCountVar, justify=CENTER)
        self.moveCountTxf.place(x=665, y=210, anchor=NW)

		# create the controls for the analysis parameters
        self.multipvLbl = Label(self, text="MultiPV:", background="#CCFF99")
        self.multipvLbl.place(x=420, y=256, anchor=NW)
        self.multipvScl = Scale(self, from_=1, to=10, length=120, orient=HORIZONTAL, background="#CCFF99", highlightbackground="#CCFF99")
        self.multipvScl.place(x=480, y=240, anchor=NW)
        self.multipvScl.set(5)

        self.depthLbl = Label(self, text="Depth:", background="#CCFF99")
        self.depthLbl.place(x=420, y=306, anchor=NW)
        self.depthScl = Scale(self, from_=2, to=20, length=120, orient=HORIZONTAL, background="#CCFF99", highlightbackground="#CCFF99")
        self.depthScl.place(x=480, y=290, anchor=NW)
        self.depthScl.set(10)
		
        self.timeLbl = Label(self, text="Time:", background="#CCFF99")
        self.timeLbl.place(x=615, y=256, anchor=NW)
        self.timeScl = Scale(self, from_=0, to=5000, length=120, orient=HORIZONTAL, background="#CCFF99", highlightbackground="#CCFF99")
        self.timeScl.place(x=666, y=240, anchor=NW)

        self.movesLbl = Label(self, text="Plies:", background="#CCFF99")
        self.movesLbl.place(x=615, y=306, anchor=NW)
        self.movesScl = Scale(self, from_=3, to=3, length=120, orient=HORIZONTAL, background="#CCFF99", highlightbackground="#CCFF99")
        self.movesScl.place(x=666, y=290, anchor=NW)

        self.threadsLbl = Label(self, text="Threads:", background="#CCFF99")
        self.threadsLbl.place(x=615, y=356, anchor=NW)
        self.threadsScl = Scale(self, from_=1, to=128, length=120, orient=HORIZONTAL, background="#CCFF99", highlightbackground="#CCFF99")
        self.threadsScl.place(x=666, y=340, anchor=NW)
        self.threadsScl.set(4)
		
        #self.sideLbl = Label(self, text="Side:", background="#CCFF99")
        #self.sideLbl.place(x=420, y=350, anchor=NW)
        #self.sideVar = IntVar()
        #self.sideVar.set(3)
        #self.sideRbn1 = Radiobutton(self, text="White", variable=self.sideVar, value=1, width=17, background="#CCFF99", justify="left").place(x=450, y=337, anchor=NW)
        #self.sideRbn2 = Radiobutton(self, text="Black", variable=self.sideVar, value=2, width=17, background="#CCFF99", justify="left").place(x=449, y=355, anchor=NW)
        #self.sideRbn3 = Radiobutton(self, text="Both",  variable=self.sideVar, value=3, width=17, background="#CCFF99", justify="left").place(x=447, y=373, anchor=NW)

        self.eventBtn = Button(self, text=u"Stop Analysis", command=self.onStopAnalysisBtnClick, width=15, height=3, background="#FFAA55")
        self.eventBtn.place(x=100, y=337, anchor=NW)
		
        self.eventBtn = Button(self, text=u"Analyse Data", command=self.onAnalysisBtnClick, width=24, height=3, background="#FFAA55")
        self.eventBtn.place(x=230, y=337, anchor=NW)

    def doGame(self, game, multipv, depth, movetime, moves, threads):
        side = 'both'
        gameMoves = list(sum(game.moves, ()))
        gameMoves = [x for x in gameMoves if x != '']
        #print(multipv, depth, movetime, moves, side, threads)
        #print(gameMoves, moves)
        self.parent.engine.put('ucinewgame')
        self.parent.engine.get()
        #if side == 'white' or side == 'both': start = 1
        #else: start = 0
        #if side == 'both': step = 1
        #else: step = 2
        with Database() as database:
            if database.addGame(game.getBasicInfo()) == False:
                print('game cannot be inserted into the database - analysis will not be logged')
                return
            analysisInfo = []
            analysisInfo.append(game.id)
            analysisInfo.append(side)
            analysisInfo.append(multipv)				
            analysisInfo.append(movetime)
            analysisInfo.append(depth)				
            analysisInfo.append(moves)
            analysis_id = database.addAnalysis(tuple(analysisInfo))			
            if analysis_id == -1:			
                print('analysis info cannot be inserted into the database - results will not be logged')
                return
            t1 = datetime.now()
            results = []	
            for i in range(0, moves, 1):
                position = ' '.join(gameMoves[0:i+1])
                #parameters = self.getParameters(i, gameMoves)
                print 'processing', position
                player, topMovesforNext, otherMovesforNext = self.parent.engine.process(position, multipv, depth, movetime, threads)
                if i % 2 == 0: player_side = 'white'
                else: player_side = 'black'
                with Board() as board:
                    board.set_state(position)
                    pieces = board.get_no_of_pieces()
                results.append((i, player_side, player, pieces, topMovesforNext, otherMovesforNext))
                time.sleep(10.0/1000.0)
                if self.stopAnalysis == True:
                    print "*** Stopping analysis..."
                    break
            if self.stopAnalysis == False:
                for j in range(1, len(results), 1):
                    i, player_side, player, pieces, _, _ = results[j]	
                    _, _, _, _, topMoves, otherMoves = results[j - 1]
                    playerValue = player[0]	
                    playerMove = player[1]
                    rank = self.rankMove(topMoves, playerMove)                 				
                    result_id = database.addResults((analysis_id, playerMove, playerValue, rank, player_side, pieces, i))				                   
                    if result_id == -1:
                        print('player results cannot be inserted into the database - engine moves will not be logged')
                    else:
                        topMoves = [(result_id, x[1][1], x[1][0], x[0], depth) for x in topMoves]
                        if database.addSuggestions(topMoves) == False:
                            print('engine top moves cannot be inserted into the database') 
                        #print(computerOther) 
                        otherMoves = [(result_id, x[1], x[0], -1, x[2]) for x in otherMoves]
                        if database.addSuggestions(otherMoves) == False:
                            print('engine other moves cannot be inserted into the database') 
                    #print self.parent.engine.toString(result), '\n'                    
            t2 = datetime.now()
            delta = t2-t1		
            print 'time elapsed:', delta

    def rankMove(self, moves, move):
        rank = len(moves) + 1
        ranks = [x[0] for x in moves if x[1][1]==move]
        if ranks != []: rank = ranks[0]
        return rank
				
    def onStopAnalysisBtnClick(self):
        self.stopAnalysis = True

    def onAnalysisBtnClick(self):
        self.stopAnalysis = False
        multipv = int(self.multipvScl.get())
        depth = int(self.depthScl.get())
        movetime = int(self.timeScl.get())
        moves = int(self.movesScl.get())
        threads = int(self.threadsScl.get())
        #if self.sideVar.get() == 1: side = 'white'
        #elif self.sideVar.get() == 2: side = 'black'
        #else: side = 'both'
        def doGames():
            for game in self.parent.games:
                gameMoves = list(sum(game.moves, ()))
                gameMoves = [x for x in gameMoves if x != '']
                moves = len(gameMoves)
                self.doGame(game, multipv, depth, movetime, moves, threads)
                if self.stopAnalysis == True:
                    thread.exit()
        if self.allGamesVar.get() == 1:
            try:
                thread.start_new_thread(doGames, ())
            except:
                print "Error: unable to start thread"
            return
        elif self.selectedGame == "":
            print('there is no game selected')
            return
        game = self.parent.games[self.selectedGame]
        try:
            thread.start_new_thread(self.doGame, (game, multipv, depth, movetime, moves, threads, ))
        except:
            print "Error: unable to start thread"

    def getParameters(self, i, gameMoves):
        position = ' '.join(gameMoves[0:i+1])
        #playerMove = ''.join(gameMoves[i+1:i+2])
        #return (position, playerMove)
        return position

    def getParameters2(self, position, gameMoves, side):
        moveList = gameMoves[0:position+1]
        previousMoves = moveList[0:position]
        previousMoves = ' '.join([' '.join(x) for x in previousMoves])
        if side == 'w':
            currentMove = moveList[position][0]
        else:
            previousMoves = previousMoves + ' ' + moveList[position][0]
            currentMove = moveList[position][1]
        return (previousMoves, currentMove)
		
    def onEventBtnClick(self):
        items = self.eventLst.curselection()
        items = "".join([self.events[int(item)] for item in items])
        self.selectedEvent = items
        self.selectedSite = ""
        self.updateGames()

    def onAllGamesBtnClick(self):
        self.selectedEvent = ""
        self.selectedSite = ""
        self.updateGames()
		
    def onSiteBtnClick(self):
        items = self.siteLst.curselection()
        items = "".join([self.sites[int(item)] for item in items])
        self.selectedSite = items
        self.selectedEvent = ""
        self.updateGames()

    def onGameBtnClick(self):
        items = self.gameLst.curselection()
        if (not items): return
        items = self.games[int(items[0])]
        self.selectedGame = items
        game = self.parent.games[self.selectedGame]
        self.gameEventVar.set(game.getInfo('Event'))
        self.gameSiteVar.set(game.getInfo('Site'))
        self.gameDateVar.set(game.getInfo('Date'))
        self.gameRoundVar.set(game.getInfo('Round'))
        self.gameWhiteVar.set(game.getInfo('White'))
        self.gameBlackVar.set(game.getInfo('Black'))
        self.gameResultVar.set(game.getInfo('Result'))
        self.updateGameMoves(game.moves)
        self.moveCountVar.set(len(game.moves))
        gameMoves = list(sum(game.moves, ()))
        gameMoves = [x for x in gameMoves if x != '']
        self.movesScl.config(to=len(gameMoves))
		
    def updateEvents(self,events):
        #delete all pevious items
        self.eventLst.delete(0, END)
		
		#load new events
        self.events = list(events)
        for item in self.events:
            self.eventLst.insert(END, item)

    def updateSites(self,sites):
        #delete all pevious items
        self.siteLst.delete(0, END)
		
		#load new sites
        self.sites = list(sites)
        for item in self.sites:
            self.siteLst.insert(END, item)
			
    def updateGames(self):
        #delete all previous items
        self.gameLst.delete(0, END)
		
        #gather info regarding players
        players = []
		
		#load new games
        self.games = []
        for i, item in enumerate(self.parent.games):
            if (self.selectedEvent != ""):
                if (item.getInfo("Event") == self.selectedEvent):
                    self.gameLst.insert(END, item.id)
                    self.games.append(i)
            elif (self.selectedSite != ""):
                if (item.getInfo("Site") == self.selectedSite):
                    self.gameLst.insert(END, item.id)
                    self.games.append(i)
            else:
                self.gameLst.insert(END, item.id)
                self.games.append(i)
            players.append(item.getInfo("White"))
            players.append(item.getInfo("Black"))

        #find most common player in the file
        import collections
        player = collections.Counter(players).most_common(1)[0][0]
        #print('***', player)
        self.commonPlayerVar.set(player)

    def updateGameMoves(self,moves):
        #delete all previous items
        self.moveLst.delete(0, END)

		#load new moves
        for item in ["["+item[0]+","+item[1]+"]" for item in moves]:
            self.moveLst.insert(END, item)

    def resetControls(self):
        self.selectedEvent = ""
        self.selectedSite = ""
        self.gameEventVar.set('')
        self.gameSiteVar.set('')
        self.gameDateVar.set('')
        self.gameRoundVar.set('')
        self.gameWhiteVar.set('')
        self.gameBlackVar.set('')
        self.gameResultVar.set('')
        self.updateGameMoves([])
        self.moveCountVar.set('')
		
class OptionsPage(Frame):
    def __init__(self,parent):
        Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        pass
		
def main():
    app = MainApp()
    app.title('Chess Analyser')
    app.mainloop()

if __name__ == "__main__":
    main()
