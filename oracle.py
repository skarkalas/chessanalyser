import cx_Oracle

class Database:
    def __enter__(self):
        self.database = cx_Oracle.Connection('APEX_CHESSCHEAT/Cheat1234@bbk')
        self.cursor = self.database.cursor()
        print('connected to database')
        return self
		
    def __exit__(self, type, value, traceback):
        self.database.close()
        print('disconnected from database')

    def addGame(self, info):
        #print(info)
        select_game = "select * from games where hash = :id"
        insert_game = 'insert into games values (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10)'
        hash = info[0]
        self.cursor.execute(select_game, id=hash)
        rows = self.cursor.fetchall()
        if self.cursor.rowcount == 0:
            self.cursor.execute(insert_game, info)
        if self.cursor.rowcount == 1:
            print('game inserted:', hash)
            self.database.commit()
            return True
        else:
            return False
		
    def addAnalysis(self, info):
        analysis_id = self.cursor.var(cx_Oracle.NUMBER)
        insert_analysis = "insert into analyses (id, game_id, player, multipv, time_, depth_, moves) values (analyses_seq.nextval, %d, '%s', %d, %d, %d, %d) returning id into :newid" % info
        self.cursor.prepare(insert_analysis)
        self.cursor.execute(None, newid=analysis_id)
        analysis_id = int(analysis_id.getvalue())
        if self.cursor.rowcount == 1:
            self.database.commit()
            return analysis_id
        else:
            return -1
			
    def addResults(self, info):
        results_id = self.cursor.var(cx_Oracle.NUMBER)
        insert_result = "insert into results (id, analysis_id, player_move, value_, rank_, player, pieces, move_no) values (results_seq.nextval, %d, '%s', %d, %d, '%s', %d, %d) returning id into :newid" % info
        self.cursor.prepare(insert_result)
        self.cursor.execute(None, newid=results_id)
        results_id = int(results_id.getvalue())
        if self.cursor.rowcount == 1:
            self.database.commit()
            return results_id
        else:
            return -1

    def addSuggestions(self, info):
        insert_suggestions = 'insert into suggestions (id, result_id, engine_move, value_, rank_, depth_) values (suggestions_seq.nextval, :1, :2, :3, :4, :5)'
        self.cursor.prepare(insert_suggestions)
        try:
            self.cursor.executemany(None, info)
            assert self.cursor.rowcount != 0
        except AssertionError:
            self.database.rollback()
            raise Warning, 'engine suggestions are not inserted'
            return False
        self.database.commit()
        return True

def main():
    #pass
    with Database() as database:
        database.addGame((1381657526, 'Lloyds Bank op', 'London', '1984.??.??', '1', 'Adams, Michael', 'Sedgwick, David', '1-0'))

if __name__ == "__main__":
    main()
				
				
				

